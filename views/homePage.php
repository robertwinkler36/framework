<?php

/* 
 * This a home page shell.
 * Note the login check function called on page load.
 * This is the basic security built into the page.
 */

include_once $_SERVER["DOCUMENT_ROOT"] . '/framework/engine.php';
isLoggedIn(); //checks if user navigating to this page is logged in.
?>
<html>
    <head>
        
    </head>
    <body>
        Home Page
    </body>
</html>