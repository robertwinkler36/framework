<?php
/*
 * Include all controllers and models.
 * Every file will include this engine so all models and controllers can be accessed.
 */
//include_once 'models/sample.php';
include_once 'models/user.php';
include_once 'controllers/userController.php';

//Begin your session
session_start();

//Setup database connection
$mysqli = mysqli_connect("localhost", "root", "root", "framework", 3306);


//Set encryption salt
$salt = "thelifeofthewifeisendedbytheknife";

//Get all uri's as they are hit.
$uri = $_SERVER['REQUEST_URI']; 

//Declare base url for the application.
$baseUrl = "http://localhost/framework/";

/*
 * Array mapping uri to function names.
 * This will work with the htaccess file to call the correct function in a controller when a uri is mapped there.
 * To add a mapped function: $uriMap["/baseUri/uriToMap"] = "functionName"
 */
$uriMap = array();
$uriMap["/framework/register"] = "registerUser"; //function in userController.php
$uriMap["/framework/login"] = "loginUser"; //function in userController.php

//Declares the function name attached to the uri if there is one.
//This variable will be called in the appropriate class and initialize the desired function.
$func = uriMapper();

/**
 * $location param is the uri 
 */
function forward($location)
{
    global $baseUrl;
    
    if (!headers_sent()){
        header("Location: " . $baseUrl . $location);
        exit;
    }else{
        echo "Error trying to forward to next page.";
    }
}

/**
 * Checks if user object is set in session and logged in.
 * @return boolean
 */
function isLoggedIn()
{
    if (!isset($_SESSION['user']))
    {
        forward("loginUser");
    }else{
        $token = new token();
        $token->setToken($_SESSION["user"]->getToken());
        $token->tokenTimeout();
        $valid = $token->verifyToken();
        if(!$valid){
            forward("loginUser");
        }else{
            // update last action time on token
            $token->tokenUpdateLastAction();
            return true;
        }
    }
}



/**
 * Use this function to declare endpoint functions for the uri.
 *  Uri's are mapped in a yaml file in the root directory of the application.
 */
function uriMapper()
{
    global $uriMap;
    global $uri;
    $func = "";
    $uriArray = explode("?", $uri);
    $sanitizedUri = $uriArray[0];
    
    if(isset($uriMap[$sanitizedUri])){
        $func = $uriMap[$sanitizedUri];
    }
    
    
    return $func;
    
}


/**
 * Displays a view.
 * @param $view The name of the view file.
 * @param $viewArgs Any args that need to be passed into the view file.
 * @return type
 */
function getView($view, $viewArgs)
{
	ob_start(); // Start output buffering
	include_once "views/" . $view;

	$list = ob_get_contents(); // Store buffer in variable

	ob_end_clean(); // End buffering and clean up
	
	return $list;
}



?>
