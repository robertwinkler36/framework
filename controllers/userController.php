<?php

/* 
 * This is a sample user controller.  It is already linked to a user model via the engine.
 * This controller is set to handle basic user creation and login.
 */

include_once $_SERVER["DOCUMENT_ROOT"] . '/framework/engine.php';

if(isset($func)){
    $func();
}



/**
 * Takes in new user information and uses the model to create a new user in the database.
 * 
 * TODO: Forward new user to login page with success message.
 */
function registerUser(){
    
    $user = new user();
    $user->setEmail($_POST["email"]);
    $user->setPassword($_POST["password"]);
    $user->setUsername($_POST["username"]);
    $user->create();
    echo getView("userLogin.php", null);
}

/**
 * Takes in submitted credentials.
 * Verifies user is real.
 * Assigns login token for site security
 * Puts user object in session
 * 
 * TODO: Assign a proper landing page after login.s
 */
function loginUser(){
    $email = $_POST["email"];
    $password = $_POST["password"];
    
    $user = new user();
    $user->setEmail($email);
    $user->setPassword($password);
    $user->login();
    echo getView("homePage.php", null);
}