<?php

declare(strict_types=1);

include_once __DIR__.'/../engine.php';
use PHPUnit\Framework\TestCase;

final class UserTest extends TestCase{
    
    public function testSetUsername() : void{
        $user = new User();
        $user->setUsername("Rob");
        $this->assertEquals("Rob", $user->getUsername());
    }
    
    
}

    

