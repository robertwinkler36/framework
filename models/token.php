<?php

/*
 * TODO:  Create standard select, update queries so all we have to do is pass in the objects.
 * This model creates and handles login tokens to manage user session timeouts.
 */
include_once $_SERVER["DOCUMENT_ROOT"] . '/framework/engine.php';

/**
 * Description of token
 *
 * @author rWinkler
 */
class token {
    private $id;
    private $token; //string
    private $creationTime; //timestamp
    private $lastAction; //timestamp
    private $valid; //Boolean
    
    /**
     * Returns the token.
     * @return type
     */
    function getToken(){
        return $this->token;
    }
    
    /**
     * Returns the last time an action was taken with this token.
     * @return type
     */
    function getLastAction(){
        return $this->lastAction;
    }
    
    /**
     * Returns true/false of token's validity.
     * @return type
     */
    function getValid(){
        return $this->valid;
    }
    
    /**
     * Sets the token.
     * @param type $x
     */
    function setToken($x){
        $this->token = $x;
    }
    
    /**
     * Sets the last time an action was taken with this token.
     * @param type $x
     */
    function setLastAction($x){
        $this->lastAction = $x;
    }
    
    /**
     * Sets true/false of token's validity.
     * @param type $x
     */
    function setValid($x){
        $this->valid = $x;
    }
    
    /**
     * Creates a random token and enters it into the database.
     * @global type $mysqli
     */
    function create(){
        global $mysqli;
        $this->token = uniqid(rand());
        $query = "INSERT INTO token (token) VALUES ('$this->token')";
        mysqli_query($mysqli, $query);
    }
    
    /**
     * Verify token is valid in database.
     * @global type $mysqli
     * @return boolean
     */
    function verifyToken(){
        global $mysqli;
        $query = "SELECT id FROM token WHERE token = '$this->token' AND valid = TRUE";
        $result = mysqli_query($mysqli, $query);
        
        if(mysqli_num_rows($result) > 0){ //There should only be one row returned.
            return TRUE;            
        }elseif(mysqli_num_rows($result) == 0){ //No user returned
            return false;
        }else{ //More than one user returned
            return false;
        }
    }
    
    /**
     * Compares current time to time of last action by user and if more than 30 minutes, token is invalidated.
     * @global type $mysqli
     * @return boolean
     */
    function tokenTimeout(){
        global $mysqli;
        $query = "SELECT lastAction FROM token WHERE token = '$this->token' AND valid = TRUE";
        $result = mysqli_query($mysqli, $query);
        
        if(mysqli_num_rows($result) == 1){ //There should only be one row returned.
            $row = $result->fetch_assoc();
            $lastAction = $row["lastAction"];
            $currentTime = new DateTime("now");
            $lastActionTime = date_create($lastAction);
            $diff = date_diff($currentTime, $lastActionTime);
            
            if($diff->i  > 30){  //User has been idle for over 30 minutes
                //invalidate token                
                $query = "UPDATE rpg_token SET valid = FALSE WHERE token = '$this->token' AND valid = TRUE";
                mysqli_query($mysqli, $query);
            }
            return TRUE;            
        }elseif(mysqli_num_rows($result) == 0){ // no last action returned;
            return false;
        }else{ //More than one last action time returned
            return false;
        }
    }
    
    
    /**
     * Token's lastAction time is updated as user moves through the application.
     * @global type $mysqli
     * @return boolean
     */
    function tokenUpdateLastAction(){
        global $mysqli;
        
        $query = "UPDATE token SET lastAction = CURRENT_TIMESTAMP WHERE token = '$this->token' AND valid = TRUE";
        mysqli_query($mysqli, $query);
        
    }
}
