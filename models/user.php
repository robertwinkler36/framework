<?php

/*
 * This is a basic user model wired in through the engine.
 * This is set up to handle basic user model queries.
 * TODO:  Create standard select, update queries so all we have to do is pass in the objects.
 */
include_once $_SERVER["DOCUMENT_ROOT"] . '/framework/engine.php';


/**
 * Description of user
 *
 * @author rWinkler
 */
class user {
    private $id;
    private $username;
    private $password;
    private $email;
    private $token;
    
    /**
     * Returns id as int
     * @return type
     */
    function getId(){
        return $this->id;
    }
    
    /**
     * Returns username as string
     * @return type
     */
    function getUsername(){
        return $this->username;
    }
    
    /**
     * Returns email as string
     * @return type
     */
    function getEmail(){
        return $this->email;
    }
    
    /**
     * Returns login token
     * @return type
     */
    function getToken(){
        return $this->token;
    }
    
    /**
     * Set string to username
     * @param type $x
     */
    function setUsername($x){
        $this->username = $x;
    }
    
    /**
     * Set string to password
     * @param type $x
     */
    function setPassword($x){
        $this->password = $x;
    }
    
    /**
     * Set string to email
     * @param type $x
     */
    function setEmail($x){
        $this->email = $x;
    }
    
    /**
     * Sets login token
     * @param type $x
     */
    function setToken($x){
        $this->setToken($x);
    }
    
    /**
     * Creates a user in the db
     */
    function create(){
        global $mysqli;
        $query = "INSERT INTO user (username, password, email) VALUES ('$this->username', '$this->password', '$this->email')";
        mysqli_query($mysqli, $query);
    }
    
    /**
     * Searches for user's credentials in database.
     * Puts user object in session with security token.
     * TODO: Create proper returns for no user and multiple users returned.
     */
    function login(){
        global $mysqli;
        $query = "SELECT id, username FROM user WHERE password = '$this->password' AND email = '$this->email'";
        $result = mysqli_query($mysqli, $query);
        
        if(mysqli_num_rows($result) == 1){ //There should only be one row returned.
            $row = $result->fetch_assoc();
            $this->username = $row["username"];
            $this->id = $row["id"];
            $token = new token();
            $token->create();
            $this->token = $token->getToken();
            
            //This is a problem.  The password will be put into session like this!!!!!!
            $_SESSION["user"] = $this;
        }elseif(mysqli_num_rows($result) == 0){ //No user returned
            return false;
        }else{ //More than one user returned
            return false;
        }
    }
}
